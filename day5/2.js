const readline = require("readline");
const process = require("node:process");

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

const stacks = [];
let rows = [];

let parserMode = "crates";

rl.on("line", (data) => {
  if (data.match(/^ \d/)) {
    parserMode = "labels";
  }

  if (data.match(/^\s*$/)) {
    parserMode = "commands";
    return;
  }

  if (parserMode === "crates") {
    const cratesInRow = data.match(/((   |\[[A-Z]]) ?)/gm);
    rows.push(cratesInRow.map(c => c.replace(/[\[\] ]/g, "")));
  } else if (parserMode === "labels" ) {
    const stackLabels = data.match(/(( \d+ ) ?)/gm);
    const stackIds = stackLabels.map(l => parseInt(l.replace(/ /g, "")));
    stackIds.forEach((s) => {
      for (let i = rows.length - 1; i >= 0; i--) {
        if (i[s - 1] !== '') {
          if (!stacks[s]) stacks[s] = [];
          if (rows[i][s - 1] !== '') {
            stacks[s].push(rows[i][s - 1]);
          }
        }
      }
    })
  } else if (parserMode === "commands") {
    let command = data.match(/move (\d+) from (\d+) to (\d+)/m);
    const [numToMove, from, to] = command.slice(1, 4).map(x => parseInt(x));
    const moving = stacks[from].slice(numToMove * -1);
    if (numToMove === stacks[from].length) {
      stacks[from] = [];
    } else {
      stacks[from] = stacks[from].slice(0, stacks[from].length - numToMove);
    }
    stacks[to].push(...moving);
  }

})

rl.on("close", () => {
  const result = stacks.flatMap(stack => stack.slice(-1));
  console.log(result.join(""));
})