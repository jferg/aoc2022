const readline = require("readline");
const process = require("node:process");

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let totalScore = 0;

const stacks = [];
let rows = [];

let parserMode = "crates";

rl.on("line", (data) => {
  if (data.match(/^ \d/)) {
    console.log("Parsing stack identifiers.");
    parserMode = "labels";
  }

  if (data.match(/^\s*$/)) {
    console.log("Parsing commands.");
    parserMode = "commands";
    console.log(stacks);
    return;
  }

  if (parserMode === "crates") {
    const cratesInRow = data.match(/((   |\[[A-Z]]) ?)/gm);
    rows.push(cratesInRow.map(c => c.replace(/[\[\] ]/g, "")));
    console.log(rows);
  } else if (parserMode === "labels" ) {
    const stackLabels = data.match(/(( \d+ ) ?)/gm);
    const stackIds = stackLabels.map(l => parseInt(l.replace(/ /g, "")));
    stackIds.forEach((s) => {
      for (let i = rows.length - 1; i >= 0; i--) {
        if (i[s - 1] !== '') {
          if (!stacks[s]) stacks[s] = [];
          if (rows[i][s - 1] !== '') {
            stacks[s].push(rows[i][s - 1]);
          }
        }
      }
    })
  } else if (parserMode === "commands") {
    let command = data.match(/move (\d+) from (\d+) to (\d+)/m);
    const [numToMove, from, to] = command.slice(1, 4).map(x => parseInt(x));
    const moving = stacks[from].slice(numToMove * -1).reverse();
    console.log(`Moving ${numToMove} (${moving}) from ${from} to ${to}.`);
    if (numToMove === stacks[from].length) {
      stacks[from] = [];
    } else {
      stacks[from] = stacks[from].slice(0, stacks[from].length - numToMove);
    }
    stacks[to].push(...moving);
    console.log(stacks);
  }

})

rl.on("close", () => {
  console.log(stacks);
  const result = stacks.flatMap(stack => stack.slice(-1));
  console.log(result.join(""));
})