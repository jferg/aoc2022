const readline = require("readline");
const process = require("node:process");

const ROCK = Symbol("rock");
const PAPER = Symbol("paper");
const SCISSORS = Symbol("scissors");

const SHAPESCORES = { [ROCK]: 1, [PAPER]: 2, [SCISSORS]: 3 };

const GUIDE = {
  A: ROCK,     X: ROCK,
  B: PAPER,    Y: PAPER,
  C: SCISSORS, Z: SCISSORS,
}

// Returns 0 if player A wins
//         3 on a draw
//         6 if player B wins
const winnerScore = (shapeA, shapeB) => {
  if ( (shapeB === ROCK && shapeA === SCISSORS) ||
       (shapeB === PAPER && shapeA === ROCK) ||
       (shapeB === SCISSORS && shapeA === PAPER ) ) {
    return 6;
  }

  if (shapeA === shapeB) { return 3; }

  return 0;
}

const scoreForRound = (myShape, theirShape) => {
  return SHAPESCORES[theirShape] + winnerScore(myShape, theirShape);
}

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let totalScore = 0;

rl.on("line", (data) => {
  const matchup = data.split(" ");
  totalScore += scoreForRound(...matchup.map(c => GUIDE[c]));
})

rl.on("close", () => {
  console.log("Total score is: ", totalScore);
})