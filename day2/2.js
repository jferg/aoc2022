const readline = require("readline");
const process = require("node:process");

const ROCK = Symbol("rock");
const PAPER = Symbol("paper");
const SCISSORS = Symbol("scissors");
const WIN = Symbol("win");
const LOSE = Symbol("lose");
const DRAW = Symbol("draw");

const SHAPESCORES = { [ROCK]: 1, [PAPER]: 2, [SCISSORS]: 3 };

const GUIDE = {
  A: ROCK,     X: LOSE,
  B: PAPER,    Y: DRAW,
  C: SCISSORS, Z: WIN,
}

const OUTCOMES = {
  [ROCK]: {
    [WIN]: PAPER,
    [DRAW]: ROCK,
    [LOSE]: SCISSORS
  },
  [PAPER]: {
    [WIN]: SCISSORS,
    [DRAW]: PAPER,
    [LOSE]: ROCK,
  },
  [SCISSORS]: {
    [WIN]: ROCK,
    [DRAW]: SCISSORS,
    [LOSE]: PAPER,
  },
}

const outcomeFromRequirement = (theirShape, requiredOutcome) => {
  return OUTCOMES[theirShape][requiredOutcome];
}


// Returns 0 if player A wins
//         3 on a draw
//         6 if player B wins
const winnerScore = (shapeA, shapeB) => {
  if ( (shapeB === ROCK && shapeA === SCISSORS) ||
       (shapeB === PAPER && shapeA === ROCK) ||
       (shapeB === SCISSORS && shapeA === PAPER ) ) {
    return 6;
  }

  if (shapeA === shapeB) { return 3; }

  return 0;
}

const scoreForRound = (theirShape, requiredOutcome) => {
  const myShape = outcomeFromRequirement(theirShape, requiredOutcome);
  return SHAPESCORES[myShape] + winnerScore(theirShape, myShape);
}

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let totalScore = 0;

rl.on("line", (data) => {
  const matchup = data.split(" ");
  totalScore += scoreForRound(...matchup.map(c => GUIDE[c]));
})

rl.on("close", () => {
  console.log("Total score is: ", totalScore);
})