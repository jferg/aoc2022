const { createReadStream } = require("node:fs");

const rs = createReadStream(process.argv[2]);

const containsDuplicates = (set) => {
  const foundChars = {};
  let hasDupe = false;

  set.forEach((c) => {
    if (foundChars[c]) {
      hasDupe = true;
    } else {
      foundChars[c] = 1;
    }
  })

  return hasDupe;
}

let positionCounter = 14;

rs.on("readable", function () {
  let start = this.read(14);
  if (start) {
    let current = Array.from(start).map((c) => String.fromCharCode(c));

    let data;
    while ((data = this.read(1)) !== null) {

      if (!containsDuplicates(current)) {
        console.log(`First marker at ${positionCounter}`);
        process.exit(0);
      }

      positionCounter += 1;
      current.push(data.toString());
      current.shift();

      console.log(current);
    }
  }
})

rs.on("end", () => {
  console.log("Thaaaaat's it, Max!");
})