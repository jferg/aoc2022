const readline = require("readline");
const process = require("node:process");

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let total = 0;
let counter = 3;

const findCommonItems = (sack1, sack2) => {
  return sack1.split("").filter(item => sack2.indexOf(item) > -1);
}

const getItemPriority = (item) => {
  const val = item.charCodeAt(0);
  if (val < 97) {
    return val - 38;
  } else {
    return val - 96;
  }
}

rl.on("line", (data) => {
  const allItems = data;
  const sack1 = allItems.substring(0, allItems.length / 2);
  const sack2 = allItems.substring(allItems.length / 2);
  total += getItemPriority(findCommonItems(sack1, sack2)[0]);
})

rl.on("close", () => {
  console.log("Total is: ", total);
})