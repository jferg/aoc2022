const readline = require("readline");
const process = require("node:process");

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let total = 0;

const findCommonItems = (sack1, sack2, sack3) => {
  return sack1.split("").filter(item => (sack2.indexOf(item) > -1 && sack3.indexOf(item) > -1));
}

const getItemPriority = (item) => {
  const val = item.charCodeAt(0);
  if (val < 97) {
    return val - 38;
  } else {
    return val - 96;
  }
}

let lines = [];
let line = 0;

rl.on("line", (data) => {
  lines[line] = data;
  line += 1;

  if (line === 3) {
    total += getItemPriority(findCommonItems(...lines)[0]);
    lines = [];
    line = 0;
  }
});


rl.on("close", () => {
  console.log("Total is: ", total);
})