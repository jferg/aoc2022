const readline = require("readline");
const process = require("node:process");

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let totalOverlapping = 0;

const isBetween = (a, range) => {
  return a >= range[0] && a <= range[1];
}

rl.on("line", (data) => {
  const ranges = data.split(",");

  const range0 = ranges[0].split("-").map(x => parseInt(x, 10));
  const range1 = ranges[1].split("-").map(x => parseInt(x, 10));

  if (
    isBetween(range0[0], range1) ||
    isBetween(range0[1], range1) ||
    isBetween(range1[0], range0) ||
    isBetween(range1[1], range0) ) {
    totalOverlapping += 1;
  }
})

rl.on("close", () => {
  console.log(`Total overlapping ranges: ${totalOverlapping}`)
})