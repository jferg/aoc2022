const readline = require("readline");
const process = require("node:process");

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let totalFullyContained = 0;

rl.on("line", (data) => {
  const ranges = data.split(",");
  console.log("ranges: ", ranges);

  const range0 = ranges[0].split("-").map(x => parseInt(x, 10));
  const range1 = ranges[1].split("-").map(x => parseInt(x, 10));

  console.log("Range 0: ", range0);
  console.log("Range 1: ", range1);

  if ((range0[0] >= range1[0] && range0[1] <= range1[1]) ||
    (range1[0] >= range0[0] && range1[1] <= range0[1])) {
    totalFullyContained += 1;
  }
})

rl.on("close", () => {
  console.log(`Total fully overlapping ranges: ${totalFullyContained}`)
})