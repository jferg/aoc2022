const process = require("node:process");
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let elfCount = 0;
// Goofy initialization of the array.
const caloriesByElf = { 0: 0 };

rl.on("line", (data) => {
  if ( data === "" ) {
    elfCount += 1;
    caloriesByElf[elfCount] = 0;
  } else {
    const calories = parseInt(data, 10);
    caloriesByElf[elfCount] += calories;
  }
})

let maxElf = 0;

rl.on("close", () => {

  const sorted = Object.values(caloriesByElf).sort((a, b) => b - a);
  const res = sorted.slice(0, 3).reduce((acc, cur) => acc + cur, 0);

  console.log(`Sum of the top three elves' calories is ${res} calories.`);

  process.exit(0);
});