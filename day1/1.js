const process = require("node:process");
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: undefined
})

let elfCount = 0;
// Goofy initialization of the array.
const caloriesByElf = { 0: 0 };

rl.on("line", (data) => {
  if ( data === "" ) {
    elfCount += 1;
    caloriesByElf[elfCount] = 0;
  } else {
    const calories = parseInt(data, 10);
    caloriesByElf[elfCount] += calories;
  }
})

let maxElf = 0;

rl.on("close", () => {
  for (const i in caloriesByElf) {
    if (caloriesByElf[i] > caloriesByElf[maxElf]) {
      maxElf = i;
    }
  }

  console.log(`Elf with the highest calories is #${maxElf}, with ${caloriesByElf[maxElf]} calories.`);

  process.exit(0);
})